from PIL import Image 
import time
width = 160
height = 120
imag= Image.open("IMG3.JPG")

#Cambio la resolucion de la imagen a VGA
imag2 = imag.resize((width, height), Image.NEAREST) 
file = open("datos_binarios.txt", "w")
#imag.show()

#convierto la imagen a escala de grises para tener un solo canal 
imag2=imag2.convert('L') #convierto la imagen con la funcion de python 

#obtengo los datos de la imagen
datos=imag2.getdata()
#creo un arreglo que obtendra los datos binarizados del la imagen
datos_binarios=[]
#binarizacion=[]

#Le calculo un umbral 
umbral=65

##for para recorrer toda la imagen y binarizarla 
for x in datos:
    if x<umbral:
        datos_binarios.append(255)
        #binarizacion.append(1)
        continue
    #si es mayor o igual a umbral se agrega 1 en ves de 0
    #podria hacerse con 255 en ves de 1
    datos_binarios.append(0)
    #binarizacion.append(0)

i=0
con=0 #contador para darle formato a los datos binarios
#for para recorrer todos los datos binarios que contenga la imagen ya procesada 
for p in datos:
	if datos_binarios[i]==0:
		if con==width:
			file.write("\n0")
			i+=1
			con=0
		else:
			file.write("0")
			i+=1
		con+=1
			
	else:
		if con==width:
			file.write("\n1")
			i+=1
			con=0
		else:
			file.write("1")
			i+=1
		con+=1


imag2.putdata(datos_binarios)
imag2.save("imagen",'BMP')
imag2.show()
imag.close()    
file.close()  
#imag3.save("imagen3",'BMP')  
